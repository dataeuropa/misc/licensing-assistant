package de.fhg.fokus.edp.licensing.resources;

import org.jboss.resteasy.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Created by aos on 6/17/15.
 */
@Path("ping")
public class PingResource {
    private final static Logger log = Logger.getLogger(PingResource.class);

    @GET
    public String ping() {
        return "pong";
    }
}
