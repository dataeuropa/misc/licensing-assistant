package de.fhg.fokus.edp.licensing.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Optional;

/**
 * Created by aos on 4/7/15.
 */
public class LicenseTerm implements Serializable {
    private String id, title, type;
    private Optional<String> description;

    public LicenseTerm(String id, String title, String type, Optional<String> description) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    @JsonIgnore
    public Optional<String> getDescription() {
        return description;
    }


    @JsonProperty("description")
    private String getDescriptionJson() {
        return description.orElse(null);
    }
}
