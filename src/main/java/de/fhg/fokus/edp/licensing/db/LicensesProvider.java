package de.fhg.fokus.edp.licensing.db;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Singleton;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

import de.fhg.fokus.edp.licensing.domain.License;
import de.fhg.fokus.edp.licensing.domain.LicenseTerm;

/**
 * Created by aos on 4/7/15.
 */
@Singleton
public class LicensesProvider {
	private final static Logger logger = LoggerFactory.getLogger(LicensesProvider.class);
	private final Map<String, License> licenses;

	public LicensesProvider() {
		OntModel model = setupModel();
		List<Pair<Individual, LicenseTerm>> licenseTerms = setupLicenseTerms(model);
		this.licenses = setupLicenses(model, licenseTerms);
	}

	public Map<String, License> getLicenses() {
		return licenses;
	}

	private Map<String, License> setupLicenses(OntModel model, List<Pair<Individual, LicenseTerm>> licenseTerms) {
		logger.trace("Loading licenses");
		Resource copyrightLicenseTemplateResource = model
				.getResource("http://europeandataportal.eu/ontologies/licenses#CopyrightLicenseTemplate");
		Property templateSourceProperty = model
				.getProperty("http://europeandataportal.eu/ontologies/od-licenses#templateSource");

		Property mtopVersionProperty = model.getProperty("http://europeandataportal.eu/ontologies/top#version");
		Property mtopNameProperty = model.getProperty("http://europeandataportal.eu/ontologies/top#name");
		Property mtopLongNameProperty = model.getProperty("http://europeandataportal.eu/ontologies/top#longName");
		Property compatibleWithProperty = model
				.getProperty("http://europeandataportal.eu/ontologies/copyright#compatibleWith");

		return model.listIndividuals(copyrightLicenseTemplateResource).toList().parallelStream().map(i -> {
			logger.trace("Processing: " + i.getURI());
			String name = i.getPropertyValue(mtopNameProperty).asLiteral().getString();
			Set<RDFNode> props = i.listProperties().toList().stream().map(p -> p.getObject())
					.collect(Collectors.toSet());
			Set<LicenseTerm> terms = licenseTerms.stream().filter(t -> props.contains(t.getLeft()))
					.map(t -> t.getRight()).collect(Collectors.toSet());

			Set<String> compatibleWith = i.listProperties(compatibleWithProperty).toList().stream()
					.map(p -> p.getResource().getLocalName()).collect(Collectors.toSet());

			return new License(i.getURI(), name, i.getLocalName(), Optional.ofNullable(i.getPropertyValue(mtopLongNameProperty)).map((RDFNode v) -> v.asLiteral().getValue().toString()), Optional.ofNullable(i.getComment(null)),
					Optional.ofNullable(i.getPropertyValue(mtopVersionProperty))
							.map((RDFNode v) -> v.asLiteral().getValue().toString()),
					Optional.ofNullable(i.getPropertyValue(templateSourceProperty))
							.map((RDFNode v) -> v.asLiteral().getValue().toString()),
					terms, compatibleWith);
		}).collect(Collectors.toMap(License::getShortName, Function.identity()));
	}

	private OntModel setupModel() {
		logger.trace("Loading model");
		OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF);

		model.read(getClass().getResourceAsStream("/ontology/top.owl"), "http://europeandataportal.eu/ontologies/top");
		model.read(getClass().getResourceAsStream("/ontology/licenses.owl"),
				"http://europeandataportal.eu/ontologies/licenses");
		model.read(getClass().getResourceAsStream("/ontology/copyright.owl"),
				"http://europeandataportal.eu/ontologies/copyright");
		model.read(getClass().getResourceAsStream("/ontology/od-licenses.owl"),
				"http://europeandataportal.eu/ontologies/od-licenses");

		logger.trace("Loading model complete");

		return model;
	}

	private List<Pair<Individual, LicenseTerm>> setupLicenseTerms(OntModel model) {
		logger.trace("Loading license features");

		Resource licenseTermResource = model
				.getResource("http://europeandataportal.eu/ontologies/copyright#LicenseTerm");

		return model.listIndividuals(licenseTermResource).toList().parallelStream().map(term -> {
			return Pair.of(term, new LicenseTerm(term.getLocalName(), term.getLocalName(), // TODO
					term.getRDFType().getLocalName(), Optional.ofNullable(term.getComment(null))));
		}).collect(Collectors.toList());
	}

}
