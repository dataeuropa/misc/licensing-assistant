package de.fhg.fokus.edp.licensing.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by aos on 8/19/15.
 */
public class CKANLicense {
    private boolean domainContent = true, domainData = true, domainSoftware = false, isGeneric = false;
    private String family = "", id, maintainer = "", odConformance = "approved", osdConformance = "not reviewed", status = "active", title, url;

    public CKANLicense(String id, String title, String url) {
        this.id = id;
        this.title = title;
        this.url = url;
    }

    @JsonProperty("domain_content")
    public boolean isDomainContent() {
        return domainContent;
    }

    public void setDomainContent(boolean domainContent) {
        this.domainContent = domainContent;
    }

    @JsonProperty("domain_data")
    public boolean isDomainData() {
        return domainData;
    }

    public void setDomainData(boolean domainData) {
        this.domainData = domainData;
    }

    @JsonProperty("domain_software")
    public boolean isDomainSoftware() {
        return domainSoftware;
    }

    public void setDomainSoftware(boolean domainSoftware) {
        this.domainSoftware = domainSoftware;
    }

    @JsonProperty("is_generic")
    public boolean isGeneric() {
        return isGeneric;
    }

    public void setIsGeneric(boolean isGeneric) {
        this.isGeneric = isGeneric;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaintainer() {
        return maintainer;
    }

    public void setMaintainer(String maintainer) {
        this.maintainer = maintainer;
    }

    @JsonProperty("od_conformance")
    public String getOdConformance() {
        return odConformance;
    }

    public void setOdConformance(String odConformance) {
        this.odConformance = odConformance;
    }

    @JsonProperty("osd_conformance")
    public String getOsdConformance() {
        return osdConformance;
    }

    public void setOsdConformance(String osdConformance) {
        this.osdConformance = osdConformance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        if (status)
            this.status = "active";
        else
            this.status = "disabled";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
