package de.fhg.fokus.edp.licensing;

import com.google.inject.Binder;
import com.google.inject.Module;
import de.fhg.fokus.edp.licensing.db.LicensesProvider;
import de.fhg.fokus.edp.licensing.resources.LicensesResource;

/**
 * Created by aos on 6/17/15.
 */
public class LicensingModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(LicensesProvider.class).asEagerSingleton();
        binder.bind(LicensesResource.class);
    }
}
