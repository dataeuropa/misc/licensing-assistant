package de.fhg.fokus.edp.licensing.resources;

import com.google.inject.Inject;
import de.fhg.fokus.edp.licensing.db.LicensesProvider;
import de.fhg.fokus.edp.licensing.domain.CKANLicense;
import de.fhg.fokus.edp.licensing.domain.License;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.io.IOException;
import java.util.*;
import java.io.InputStream;

/**
 * Created by aos on 4/7/15.
 */
@Path("/")
public class LicensesResource {
    private final static Logger log = LoggerFactory.getLogger(LicensesResource.class);

    private final static Map<String, String> ckanIdMap = createCkanMap();
    @Inject
    protected LicensesProvider licensesProvider;

    private static Map<String, String> createCkanMap() {
        Map<String, String> map = new HashMap<>();

        map.put("OGL2.0", "uk-ogl");
        map.put("DL-DE-BY1.0", "dl-de-by-1.0");
        map.put("DL-DE-BY-NC1.0", "dl-de-by-nc-1.0");
        map.put("DL-DE-BY2.0", "dl-de-by-2.0");
        map.put("DL-DE-ZERO2.0", "dl-de-zero-2.0");

        map.put("FR-LO", "fr-lo");
        map.put("ODC-ODbL", "odc-odbl");
        map.put("ODC-BY", "odc-by");

        map.put("PSEUL", "Public Sector End User Licence - INSPIRE");
        map.put("GFDL-1.3", "gfdl");

        // FIXME cc-by cc-nc cc-by-sa cc-zero  ???

        map.put("CC-BY4.0", "cc-by-4.0");
        return map;
    }

    @GET
    @Path("licenses")
    @Produces("application/json")
    public Collection<License> getLicenses() {
        return licensesProvider.getLicenses().values();
    }

    @GET
    @Path("licenses/{licenseId}")
    @Produces("application/json")
    public License getLicense(@PathParam("licenseId") String licenseId) {
        return licensesProvider.getLicenses().get(licenseId);
    }

    @GET
    @Path("ckan.json")
    @Produces("application/json")
    public List<CKANLicense> getCKANLicenses() {
        List<CKANLicense> list = new ArrayList<>();

        InputStream propsStream = this.getClass().getResourceAsStream("/la.properties");
        Properties props = new Properties();
        String url;
        try {
            props.load(propsStream);
            url = props.getProperty("base.url");
        } catch (IOException e) {
            url = "http://europeandataportal.eu";
        }

        for (String[] other : new String[][]{
                {"other-at", "Other (Attribution)"},
                {"other-open", "Other (Open)"},
                {"other-pd", "Other (Public Domain)"}
        }) {
            CKANLicense lic = new CKANLicense(other[0], other[1], "");
            lic.setDomainContent(true);
            lic.setDomainData(false);
            lic.setDomainSoftware(false);
            list.add(lic);
        }

        for (License license : licensesProvider.getLicenses().values()) {
            CKANLicense ckan = new CKANLicense(
                    ckanIdMap.getOrDefault(license.getShortName(), license.getShortName()),
                    license.getName(),
                    url + "/content/show-license?license_id=" + license.getShortName());

            list.add(ckan);
        }

        return list;
    }

}
