package de.fhg.fokus.edp.licensing;

import de.fhg.fokus.edp.licensing.resources.LicensesResource;
import de.fhg.fokus.edp.licensing.resources.PingResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by aos on 4/7/15.
 */
public class LicensingApplication extends Application {
    private final static Logger logger = LoggerFactory.getLogger(LicensingApplication.class);


    private final HashSet<Class<?>> classes = new HashSet<>();
    private final HashSet<Object> singletons = new HashSet<>();

    public LicensingApplication() {
        classes.add(LicensesResource.class);
        classes.add(PingResource.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
