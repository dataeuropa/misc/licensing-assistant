package de.fhg.fokus.edp.licensing.domain;

import java.io.Serializable;

/**
 * Created by aos on 4/7/15.
 */
public class Error implements Serializable {
    private int code;
    private String message;
    private boolean success = false;

    public Error(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Error(int code, String message, boolean success) {
        this.code = code;
        this.message = message;
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }
}
