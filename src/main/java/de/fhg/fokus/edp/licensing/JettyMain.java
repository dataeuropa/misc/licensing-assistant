package de.fhg.fokus.edp.licensing;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * Created by aos on 10/5/15.
 */
public class JettyMain {

    public static void main(String[] args) throws Exception {
        final int port = 8081;

        Server server = new Server(port);

        WebAppContext webapp = new WebAppContext();
        webapp.setDefaultsDescriptor("WEB-INF/web.xml");
        webapp.setContextPath("/licensing-assistant/api");
        webapp.setWar(".");
        webapp.setParentLoaderPriority(true);
        webapp.setServer(server);

        ResourceHandler staticResourceHandler = new ResourceHandler();
        staticResourceHandler.setDirectoriesListed(true);
        staticResourceHandler.setBaseResource(Resource.newClassPathResource("/static"));

        ContextHandler staticContextHandler = new ContextHandler();
        staticContextHandler.setContextPath("/licensing-assistant/");
        staticContextHandler.setHandler(staticResourceHandler);

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{staticContextHandler, webapp});

        server.setHandler(handlers);

        server.start();
        server.join();
    }
}
