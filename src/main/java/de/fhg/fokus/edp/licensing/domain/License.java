package de.fhg.fokus.edp.licensing.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by aos on 4/7/15.
 */
public class License implements Serializable{
    private String id, name, shortName;
    private Optional<String> description, version, url, longName;
    private Set<LicenseTerm> terms;
    private Set<String> compatibleLicenses;

    public License(String id, String name, String shortName, Optional<String> longName, Optional<String> description, Optional<String> version, Optional<String> url, Set<LicenseTerm> terms, Set<String> compatibleLicenses) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.description = description;
        this.longName = longName;
        this.version = version;
        this.url = url;
        this.terms = terms;
        this.compatibleLicenses = compatibleLicenses;
    }

    public String getId() {
        return id;
    }

    public Set<LicenseTerm> getTerms() {
        return terms;
    }

    public SortedSet<String> getCompatibleLicenses() {
        TreeSet<String> t = new TreeSet<>(compatibleLicenses);
        return t;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    @JsonIgnore
    public Optional<String> getLongName() {
        return longName;
    }

    @JsonIgnore
    public Optional<String> getDescription() {
        return description;
    }

    @JsonIgnore
    public Optional<String> getVersion() {
        return version;
    }

    @JsonIgnore
    public Optional<String> getUrl() {
        return url;
    }

    @JsonProperty("description")
    private String getDescriptionJson() {
        return description.orElse(null);
    }

    @JsonProperty("version")
    private String getVersionJson() {
        return version.orElse(null);
    }

    @JsonProperty("longName")
    private String getLongNameJson() {
        return longName.orElse(null);
    }

    @JsonProperty("url")
    private String getUrlJson() {
        return url.orElse(null);
    }

    @Override
    public String toString() {
        return "License{" +
                "shortName='" + shortName + '\'' +
                ", version=" + version +
                ", url=" + url +
                '}';
    }
}
