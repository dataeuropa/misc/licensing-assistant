let licensingAssistantLoadStart = false;

function getParameterByName(name) {
  const match = RegExp(`[?&]${name}=([^&]*)`).exec(window.location.search);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function loadLicensingAssistant($) {
  if (
      !$('#la-target').length ||
      $('#la-target').text().trim() !== '' ||
      licensingAssistantLoadStart
  ) {
    return;
  }

  licensingAssistantLoadStart = true;

  $.getScript(
      window.location.origin + '/licensing-assistant/lib.js?q3w05i',
      (data, textStatus, jqxhr) => {
        if (getParameterByName('license_id')) {
          // eslint-disable-next-line no-new
          new LicenseAssistant.ShowLicense(jQuery('#la-target'), getParameterByName('license_id'));
        } else {
          // eslint-disable-next-line no-new
          new LicenseAssistant.FilterLicenses($('#la-target'));
          $('#la-target')
              .find('h2')
              .replaceWith(function () {
                return `<blockquote>${$(this).html()}</blockquote>`;
              });
          $('#edp3 #la-target .search-box button').on('click', () => {
            $('#edp3 #la-target #filter-panel').slideToggle();
          });
        }
      }
  );

  window.showLicense = function (name) {
    document.location.href = `licensing-assistant.html?license_id=${encodeURIComponent(name)}`;
  };
}


loadLicensingAssistant($);
