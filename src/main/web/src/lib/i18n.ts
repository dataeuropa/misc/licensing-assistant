import i18next from 'i18next';
import propsEN from './i18n/en.properties!text';
import propsDE from './i18n/de.properties!text';
import propsES from './i18n/es.properties!text';
import propsFR from './i18n/fr.properties!text';
import propsIT from './i18n/it.properties!text';
import propsNL from './i18n/nl.properties!text';
import propsPL from './i18n/pl.properties!text';
import propsSK from './i18n/sk.properties!text';
import propsSV from './i18n/sv.properties!text';
import propsPT from './i18n/pt.properties!text';
import propsHU from './i18n/hu.properties!text';
import propsHR from './i18n/hr.properties!text';
import propsRO from './i18n/ro.properties!text';
import propsCS from './i18n/cs.properties!text';
import propsFI from './i18n/fi.properties!text';
import propsBG from './i18n/bg.properties!text';
import propsEL from './i18n/el.properties!text';
import propsMT from './i18n/mt.properties!text';
import propsET from './i18n/et.properties!text';
import propsGA from './i18n/ga.properties!text';
import propsDA from './i18n/da.properties!text';
import propsLV from './i18n/lv.properties!text';
import propsLT from './i18n/lt.properties!text';
import propsSL from './i18n/sl.properties!text';
import propsNO from './i18n/no.properties!text';

var lang = $("html").attr("lang") || "en";

function getTranslations(lang: string) {
    var props = {};

    switch(lang) {
    case 'en': props = propsEN; break;
    case 'de': props = propsDE; break;
    case 'es': props = propsES; break;
    case 'fr': props = propsFR; break;
    case 'it': props = propsIT; break;
    case 'nl': props = propsNL; break;
    case 'pl': props = propsPL; break;
    case 'sk': props = propsSK; break;
    case 'sv': props = propsSV; break;
    case 'pt': props = propsPT; break;
    case 'hu': props = propsHU; break;
    case 'hr': props = propsHR; break;
    case 'ro': props = propsRO; break;
    case 'cs': props = propsCS; break;
    case 'fi': props = propsFI; break;
    case 'bg': props = propsBG; break;
    case 'el': props = propsEL; break;
    case 'mt': props = propsMT; break;
    case 'et': props = propsET; break;
    case 'ga': props = propsGA; break;
    case 'da': props = propsDA; break;
    case 'lv': props = propsLV; break;
    case 'lt': props = propsLT; break;
    case 'sl': props = propsSL; break;
    case 'no': props = propsNO; break;
    }

    var lines = props.match(/[^\r\n]+/g);
    var translations = {};
    for(var line of _.filter(lines, (l) => !_.startsWith(l, "#"))) {
        var split = line.split(/=/);
        translations[split.shift()] = split.join('=');
    }
    return translations;
};

var i18opts = {
    keySeparator: '::',
    lng: lang,
    fallbackLng: 'en',
    resources: {
        'en': {translation: getTranslations('en')},
        'de': {translation: getTranslations('de')},
        'es': {translation: getTranslations('es')},
        'fr': {translation: getTranslations('fr')},
        'it': {translation: getTranslations('it')},
        'nl': {translation: getTranslations('nl')},
        'pl': {translation: getTranslations('pl')},
        'sk': {translation: getTranslations('sk')},
        'sv': {translation: getTranslations('sv')},
        'pt': {translation: getTranslations('pt')},
        'hu': {translation: getTranslations('hu')},
        'hr': {translation: getTranslations('hr')},
        'ro': {translation: getTranslations('ro')},
        'cs': {translation: getTranslations('cs')},
        'fi': {translation: getTranslations('fi')},
        'bg': {translation: getTranslations('bg')},
        'el': {translation: getTranslations('el')},
        'mt': {translation: getTranslations('mt')},
        'et': {translation: getTranslations('et')},
        'ga': {translation: getTranslations('ga')},
        'da': {translation: getTranslations('da')},
        'lv': {translation: getTranslations('lv')},
        'lt': {translation: getTranslations('lt')},
        'sl': {translation: getTranslations('sl')},
        'no': {translation: getTranslations('no')}
    }
};

i18next.init(i18opts);

export default i18next;
