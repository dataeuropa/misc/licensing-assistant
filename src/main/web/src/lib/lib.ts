import i18n from './i18n.ts!';

import {FilterLicenses} from './filter_licenses/filter_licenses.ts!';
import {LicensingWizard} from './licensing_wizard/licensing_wizard.ts!';
import {ShowLicense} from './show_license/show_license.ts!';

console.log("LA loaded. Translations loaded. " + i18n);

window.LicenseAssistant = {
    FilterLicenses: FilterLicenses,
    LicensingWizard: LicensingWizard,
    ShowLicense: ShowLicense
};
