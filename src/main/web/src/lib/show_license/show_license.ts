/**
 * Created by aos on 3/31/15.
 */
/// <reference path="../typings/tsd.d.ts" />
/// <reference path="../api.ts" />

import './show_license.scss!';
import {License, Error} from '../model.ts!';
import _tplTemplate = require('./template.jade!');
import _ from 'lodash';
import i18next from 'i18next';
import {API}from '../api.ts!';

  export class ShowLicense {
    private license:License;
      private tplOpts = {
          'imports': {
              'jq': jQuery,
              'i18n' : (p) => i18next.t(p)
          }
      };
      private template:_.TemplateExecutor = _.template(_tplTemplate.default(), this.tplOpts);

    constructor(private el:JQuery, private shortName:string) {
      API.license(shortName).then((lic) => {
        this.license = lic;
        el = el.html(this.template({
          'name': lic.name,
          'url': lic.url,
          'description': lic.description,
          'permissions': _.filter(lic.terms, (t) => t.type == "Permission"),
          'obligations': _.filter(lic.terms, (t) => t.type == "Obligation"),
          'prohibitions': _.filter(lic.terms, (t) => t.type == "Prohibition"),
          'compatibleLicenses': lic.compatibleLicenses
        }));
      }).catch((err:Error) => {
        console.dir(err);
        throw err;
      });
    }
  }
