/**
 * Created by aos on 3/31/15.
 */

export interface LicenseTerm {
  id: string;
  title: string;
  description: string
  type: string
}

export interface License {
  shortName: string;
  name: string;
  description: string;
  url: string;
  version: string;
  terms: LicenseTerm[];
  compatibleLicenses: string[];
}

export class Error {
  constructor(public code:number, public message:string, public success?:boolean) {
  }
}

export interface DecisionTreeEdge {
  child: number;
  parent: number;
  excludes: string[]; // fixme this should be a LicenseTerm[]?
  includes: string[];
}

export interface DecisionTreeNode {
  id: number;
  negative: DecisionTreeEdge;
  positive: DecisionTreeEdge;
  question: string;
}
