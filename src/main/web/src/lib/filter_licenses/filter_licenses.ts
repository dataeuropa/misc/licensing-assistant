/**
 * Created by aos on 3/31/15.
 */
/// <reference path="../typings/tsd.d.ts" />
/// <reference path="../api.ts" />
/// <reference path="../model.ts" />

import './filter_licenses.scss!';
import {License,LicenseTerm} from '../model.ts!';

import _ from 'lodash';
import i18next from 'i18next';
import {API} from '../api.ts!';

import _tplTemplate = require('./template.jade!');
import _tplLicenseRow = require('./license_row.jade!');

  export class FilterLicenses {
      private template:_.TemplateExecutor = _.template(_tplTemplate.default(), {'imports': {'jq': jQuery, 'i18n': (p) => i18next.t(p)}});
      private licenseRowTmpl:_.TemplateExecutor = _.template(_tplLicenseRow.default(), {'imports': {'jq': jQuery, 'i18n': (p) => i18next.t(p)}});
    private tableBody:JQuery;
    private nav:JQuery;
    private flag_weighted:boolean = false;

    private filterTerms:LicenseTerm[] = [];

    private styleMap = {
      "Permission": "btn-success",
      "Obligation": "btn-warning",
      "Prohibition": "btn-danger"
    };

    constructor(private el:JQuery, weighted?:boolean) {
      this.el = el.html(this.template());

      this.flag_weighted = weighted || false;
      this.tableBody = this.el.find("tbody");
      this.nav = this.el.find("nav");
      this.setupSearch();

      API.terms().then(terms => {
        var categories = _.groupBy<LicenseTerm>(terms, t => t.type);
        var newNav = $("<nav class='row'/>")
        for (var category in categories) {
            var col = $("<div class='col-md-4'><h3>" + i18next.t("law." + category.toLowerCase()) + "</h3></div>");
          categories[category].forEach((term:LicenseTerm) => {
              var cat = category;

              var catClass = 'la-' + category.toLowerCase();
              var button = $('<button type="button" class="la-button ' + catClass + ' btn btn-primary' + '">' +
                             i18next.t('law.'+term.title.toLowerCase()) + '</button>');
              button.click(e => {
                e.preventDefault();
                if (!_.includes(this.filterTerms, term)) {
                  //button.addClass("active");
                  button.removeClass("btn-primary")
                  button.addClass(this.styleMap[cat]);
                  this.filterTerms.push(term);
                } else {
                  button.blur();
                  //button.removeClass("active");
                  button.addClass("btn-primary")
                  button.removeClass(this.styleMap[cat]);

                  this.filterTerms = _.filter(this.filterTerms, t => t.id !== term.id);
                }
                this.filter();
              })
              col.append(button);
            }
          );
          newNav.append(col);
        }

        this.nav.replaceWith(newNav);
        this.nav = newNav;
      });

      this.filter();
    }

    setupSearch() {
      this.el.find("#cb_flag_weighted").change(() => this.filter());
    }

    filter() {
      API.licenses().then(promised => {
        var body = $("<tbody />");
        var filterTerms = this.filterTerms.map(t => t.id);
        var licenses = _.sortBy(promised, 'name');

        if ($("#cb_flag_weighted").is(':checked')) {
          // Perform weighted sort.
          console.log("weighted");
          licenses = _.sortBy<License>(licenses, lic => {
            var licTerms = lic.terms.map(t => t.id);
            return -_.intersection(licTerms, filterTerms).length;
          });

        } else {
          licenses = _.filter(licenses, lic => {
            var licTerms = lic.terms.map(t => t.id);
            return _.intersection(licTerms, filterTerms).length == filterTerms.length;
          });
        }


        licenses.forEach((lic:License) => {
          body.append(this.licenseRowTmpl({
            'lic': lic,
            'styles': this.styleMap
          }));
        });

        this.tableBody.replaceWith(body);
        this.tableBody = body;
      });


    }
  }
