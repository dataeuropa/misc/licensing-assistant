swagger: '2.0'
info:
  title: Licensing Assistant API
  version: "1.0.0"
host: api.euodip.eu
schemes:
  - https
basePath: /licensing/v1
produces:
  - application/json
consumes:
  - application/json
paths:
  /recommend:
    get:
      summary: License recommendation decision tree
      description: |
        The recommend endpoint returns a decision tree for guiding the user
        through the process of deciding upon a license.
        This decision tree is generated dynamically from the license ontology
        upon application start.
      parameters:
        - $ref: '#/parameters/language'
      responses:
        200:
          description: An array of decision tree nodes
          schema:
            type: array
            items:
              $ref: '#/definitions/DecisionTreeNode'
        404:
          description: Language not found
          schema:
            $ref: '#/definitions/Error'
  /licenses:
    get:
      description: |
        Returns all licenses and license terms currently contained in the ontology
      parameters:
        - $ref: '#/parameters/language'
      responses:
        200:
          description: List of licenses
          schema:
            type: array
            items:
              $ref: '#/definitions/License'
        404:
          description: Language not found
          schema:
            $ref: '#/definitions/Error'
  /licenses/{licenseId}:
    get:
      description: Returns a single license with its license terms
      parameters:
        - name: licenseId
          in: path
          required: true
          description: Short name of the license
          type: string
        - $ref: '#/parameters/language'
      responses:
        200:
          description: License
          schema:
            $ref: '#/definitions/License'
        404:
          description: Language not found
          schema:
            $ref: '#/definitions/Error'
  /analyse:
    post:
      description: Analyses the license compatibility of open data sets contained in the system
      parameters:
        - $ref: '#/parameters/language'
        - name: datasets
          in: body
          required: true
          description: Array of data set ids
          schema:
            type: array
            items:
              type: string
      responses:
        501:
          description: Not implemented.
parameters:
  language:
    name: lang
    in: query
    description: Language as ISO 639-1
    required: true
    type: string
    default: en
definitions:
  DecisionTreeEdge:
    properties:
      parent:
        type: number
      child:
        type: number
      includes:
        type: array
        items:
          type: string
      excludes:
        type: array
        items:
          type: string
  DecisionTreeNode:
    properties:
      id:
        type: number
      question:
        type: string
      positive:
        $ref: '#/definitions/DecisionTreeEdge'
      negative:
        $ref: '#/definitions/DecisionTreeEdge'
  License:
    properties:
      name:
        type: string
        description: Full license name
      shortName:
        type: string
        description: Shortened license name
      description:
        type: string
        description: Shortened license name
      version:
        type: string
        description: License number
      url:
        type: string
        description: Link to license body
      terms:
        type: array
        items:
          $ref: '#/definitions/LicenseTerm'
      compatibleLicenses:
        type: array
        description: A list of compatible licenses identified by their shortName
        items:
          type: string
  LicenseTerm:
    properties:
      id:
        type: string
      type:
        type: string
      title:
        type: string
      description:
        type: string
  Error:
    properties:
      success:
        type: boolean
      code:
        type: integer
        format: int32
      message:
        type: string
