FROM openjdk:8-jre

ENV FILE licensing-service.jar 
ENV HOME /usr/jar

EXPOSE 8081

RUN addgroup --system vertx && adduser --system --group vertx

COPY target/$FILE $HOME/

RUN chown -R vertx:vertx $HOME
RUN chmod -R a+rwx $HOME

USER vertx

WORKDIR $HOME
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar $FILE -Xms128m -Xmx1g"]
